// requires...
const fs = require('fs');
const path = require('path');
// constants...

function createFile (req, res, next) {
  // Your code to create the file.
  if (typeof req.body.filename === 'string' && typeof req.body.content === 'string') {
    fs.writeFile(path.join(__dirname, 'files', req.body.filename), req.body.content, err => {
      if (err) {
        res.status(400).send({"message" : "Please specify 'content' parameter"});
      } else {
        res.status(200).send({ "message": "File created successfully" });
      }
    });
  } else {
    res.status(400).send({"message" : "Filename and content should be string"});
  }
  
}

function getFiles (req, res, next) {
  // Your code to get all files.
  fs.readdir(path.join(__dirname, 'files'), (err, files) => {
    if (err) throw err;
    res.status(200).send({
              "message": "Success",
              "files": files});
    });
}

const getFile = (req, res, next) => {
	const filename = req.params.filename;
	if (!filename || !path.extname(filename)) {
		res.status(400).send({
			'message': 'Filename is not correct',
		})
		return;
	}
	const filePath = path.join(__dirname, 'files', filename);
	const stats = fs.statSync(filePath);
	fs.readFile(filePath, {encoding: 'utf-8'}, (err, data) => {
		if (err) {
			console.log(err);
			res.status(400).send({
				'message': `No file with "${filename}" filename found`,
			});
		} else {
			res.status(200).send({
				'message': 'Success',
				'filename': `${filename}`,
				'content': `${data}`,
				'extension': `${path.extname(filename).slice(1)}`,
				'uploadedDate': `${stats.birthtime}`,
			});
		}
	});
};




// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
